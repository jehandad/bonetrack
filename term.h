
#ifndef TERM_H
#define TERM_H

#include <termios.h>
#include <stdio.h>


void initTermios(int echo);
void resetTermios(void);
char getch_(int echo);
char getch(void);
char getche(void);



#endif // TERM_H