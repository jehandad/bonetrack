all: track

track: track.o trSerial.o term.o
	g++  track.o trSerial.o term.o -o track `pkg-config --libs opencv`

track.o: track.cpp
	g++ -c -O3 -Wall `pkg-config --cflags opencv` track.cpp

trSerial.o: trSerial.cpp
	g++ -c -O3 -Wall `pkg-config --cflags opencv` trSerial.cpp

term.o: term.cpp
	g++ -c -O3 -Wall `pkg-config --cflags opencv` term.cpp

clean:
	rm -rf *o track
