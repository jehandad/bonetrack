//
// Dependencies
//
#include "trSerial.h"

/*  Author: Jake McDermott
	Created on 5/5/14
*/

int ttySer;


void trSerInit(void)  
{
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	ttySer = open( BBB_SERIAL, O_RDWR| O_NOCTTY );


	/* Set Baud Rate */
	cfsetospeed (&tty, (speed_t)B9600);
	cfsetispeed (&tty, (speed_t)B9600);

	/* Setting other Port Stuff */
	tty.c_cflag     &=  ~PARENB;        // Make 8n1
	tty.c_cflag     &=  ~CSTOPB;
	tty.c_cflag     &=  ~CSIZE;
	tty.c_cflag     |=  CS8;

	tty.c_cflag     &=  ~CRTSCTS;       // no flow control
	tty.c_cc[VMIN]      =   1;          // read doesn't block
	tty.c_cc[VTIME]     =   5;          // 0.5 seconds read timeout
	tty.c_cflag     |=  CREAD | CLOCAL; // turn on READ & ignore ctrl lines

	/* Make raw */ // we want the command to be sent all together
	cfmakeraw(&tty);

	/* Flush Port, then applies attributes */
	tcflush( ttySer, TCIFLUSH );
	if ( tcsetattr ( ttySer, TCSANOW, &tty ) != 0)
		printf("Error setting attribute\n");

}


// Write:

void trSetSpeed(uint8_t motor, int16_t pos)
{

	char cmd[7] = {0};
	uint8_t i = 0;
	int16_t sp = pos;

	//map the motor origin to the image origin
	if(motor == AZIMUTH_MOTOR)
	{
		sp = 180 - sp;
		if(sp > 180)
			sp = 180;
		else if(sp < 0)
			sp = 0;
	}
	else if(motor == ELEV_MOTOR)
	{
		if(sp > 150)
			sp = 150;
		else if( sp < 60)
			sp = 60;
	}
	
	sprintf(cmd,"$%d%d&",motor,sp);
	printf(cmd);printf("\n");
	//probably we dont need the loop
	for(i = 0; i < 7;i++)
	   write( ttySer, &cmd[i], 1 );
}

void trWrite(char* str)
{
	write(ttySer, str, strlen(str));
}