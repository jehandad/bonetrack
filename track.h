#ifndef TRACK_H
#define TRACK_H

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <math.h>



#include <opencv2/opencv.hpp>
#include "trSerial.h"
#include "term.h"

#define MIN_HSV cvScalar(0,111,144)
#define MAX_HSV cvScalar(23, 255,297)

/** Data structure to hold the position of the ball
*/

struct trBallPos
{
	int16_t x;
	int16_t y;
};

/** Data structure to hold the position of the motor, which is more descriptive as azimuth and elevation rather than x and y coords 

*/
struct trMotorPos
{
	int16_t az; //Azimuth position of the motor
	int16_t el; //elevation position of the motor
};
//Function declerations

/** @function InitCam
	@return 	returns the initialized Open CV cam object
	Initializes the camera, opens it and sets the resolution, pixel format and any other necessary settings
*/
CvCapture* trInitCam(void);

/** @function trSavePng
	@filename The filename of the created file
	@frame, the open cv IplImage object which holds the image data
	@idx, the integer constant that is appended to the file name to save consecutive files 
	This function saves the passed open cv image object and saves it in a png file format
*/
void trSavePng(char* filename, IplImage* frame, int32_t idx = -1);

/** @function trGetPos
	@param capture OpenCV CvCapture object which holds the camera handle
	@param fr	OpenCV IplImage object which has the frame from the camera
	@param pos	returned estimated position of the object
	This function detects the point of the interest in the passed image and returns it as a point 
*/
int8_t trGetPos(CvCapture* capture, IplImage* fr, trBallPos* pos);
/** @function trSetMotorPos 
	@param pos Position of the ball in the image
	@param mot Previous position of the motor, updated to newly calculated position
	
	This function converts the image coordinates to the motor coordinates based on the passed previous position of the motor and updates the pointer to the motor position.
	
*/
int8_t trSetMotorPos(trBallPos pos, trMotorPos* mot);
#endif