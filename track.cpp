// track.cpp : Defines the entry point for the console application.
//

#include "track.h"

uint32_t idx = 0; //index of the file we will be saving

const float AzGain = 0.03;
//~~const float ElGain = 0.02;
const float ElGain = 0.02;


#define ORIGIN_X 160
#define ORIGIN_Y 100
#define AZIMUTH_HYST 0.3
#define ELEV_HYST 0.2
#define AZIMUTH_DELTA 3
#define ELEV_DELTA 2
#define MAX_ELEV 110
#define MIN_ELEV 40
#define MAX_AZI 180
#define MIN_AZI 0

int main()
{
	// Initialize capturing live feed from the camera
	// Initialize capturing live feed from the camera

	// Will hold a frame captured from the camera
	IplImage* frame = 0;
	CvCapture* capture = 0;
	/*struct timespec start, end;
	double difference = 0;*/
	//the position of the object
	trBallPos ballPos;
	ballPos.x = 0;
	ballPos.y = 0;
	trMotorPos motorPos;
	motorPos.az = 0;
	motorPos.el = 0;
	
	//Init the cam
	capture = trInitCam();
	//Init the motor control module
	trSerInit();
	trSetSpeed(AZIMUTH_MOTOR,90);//the centre of the image
	trSetSpeed(ELEV_MOTOR,90);
	motorPos.az = 90;
	motorPos.el = 90;
	
	// Couldn't get a device? Throw an error and quit
	if(!capture)
    {
        printf("Could not initialize capturing...\n");
        return -1;
    }

	// An infinite loop


	while(true)
    {
		idx++; //increment the index so we can save the images by number
		
		int8_t res = trGetPos(capture, frame, &ballPos);
		if(res == -1)
			printf("Error acquiring position of the object\n");
		if((ballPos.x == 0) && (ballPos.y == 0))
			continue;
		// Print it out for debugging purposes
		printf("%d:position (%d,%d)\n", idx,ballPos.x, ballPos.y);
		//Modulate the error with a gain and send the error command to the motors
		trSetMotorPos(ballPos,&motorPos);
		printf("%d:motor (%d,%d)\n", idx,motorPos.az, motorPos.el);
		
		
	}
	
	cvReleaseImage(&frame);
	cvReleaseCapture(&capture);
    return 0;
}

int8_t trSetMotorPos(trBallPos pos, trMotorPos* motorpos)
{
	uint16_t azMotor = (uint16_t)abs(floor(AzGain * (pos.x - ORIGIN_X)));
	
	/*if(azMotor > AZIMUTH_DELTA)
		azMotor = AZIMUTH_DELTA;*/
	
	printf("azMotor = %d\n",azMotor);
	if(pos.x > ORIGIN_X)
	{
		if(azMotor > AZIMUTH_HYST)
			motorpos->az = motorpos->az + azMotor;
		//printf("new pos %d\n",motorpos->az);

	}
	else if(pos.x < ORIGIN_X)
	{
		if(azMotor > AZIMUTH_HYST)
			motorpos->az = motorpos->az - azMotor;
		//printf("new pos %d\n",motorpos->az);

	}
	if(motorpos->az < MIN_AZI)
		motorpos->az = MIN_AZI;
	else if(motorpos->az > MAX_AZI)
		motorpos->az = MAX_AZI;

	uint16_t elMotor = (uint16_t)abs(floor(ElGain * (pos.y - ORIGIN_Y)));
	
	/*if(elMotor > ELEV_DELTA)
		elMotor = ELEV_DELTA;*/
	
	printf("elMotor = %d\n",elMotor);
	if(pos.y < ORIGIN_Y)
	{
		if(elMotor > ELEV_HYST)
			motorpos->el = motorpos->el + elMotor;
		//printf("new pos %d\n",motorpos->el);
;
	}
	else if(pos.y > ORIGIN_Y)
	{
		if(elMotor > ELEV_HYST)
			motorpos->el = motorpos->el - elMotor;
		//printf("new pos %d\n",motorpos->el);

	}

	if(motorpos->el < MIN_ELEV)
		motorpos->el = MIN_ELEV;
	else if(motorpos->el > MAX_ELEV)
		motorpos->el = MAX_ELEV;

	trSetSpeed(AZIMUTH_MOTOR,motorpos->az);
	trSetSpeed(ELEV_MOTOR,motorpos->el);
	
	return 0;
}
int8_t trGetPos(CvCapture* capture, IplImage* fr, trBallPos* pos)
{
	static uint16_t ii = 0;
	
	fr = cvQueryFrame(capture);

	// If we couldn't grab a frame... quit
	if(!fr)
		return -1;

	//~~trSavePng("orig",fr,ii);
	//convert the image to hsv to see what its like
	cvCvtColor(fr, fr, CV_BGR2HSV);
	

	IplImage* imgTh = cvCreateImage(cvGetSize(fr), 8, 1);
	//Find the pixels that are in range
	cvInRangeS(fr, MIN_HSV, MAX_HSV, imgTh);

	// Calculate the moments to estimate the position of the ball
	CvMoments *moments = (CvMoments*)malloc(sizeof(CvMoments));
	cvMoments(imgTh, moments, 1);

	// The actual moment values
	double moment10 = cvGetSpatialMoment(moments, 1, 0);
	double moment01 = cvGetSpatialMoment(moments, 0, 1);
	double area = cvGetCentralMoment(moments, 0, 0);

	pos->x = moment10/area;
	pos->y = moment01/area;
	
	delete moments;
	cvReleaseImage(&imgTh);
	ii++;
	return 0;
}

void trSavePng(char* filename, IplImage* frame, int32_t idx)
{
	char str[256] = {0};
	if(idx != -1)
		sprintf(str,"%s%d.png",filename,idx);
	else
		sprintf(str,"%s.png",filename);
		
	cvSaveImage(str,frame);
	return;
}



CvCapture* trInitCam(void)
{
	CvCapture* capture = 0;
	capture = cvCaptureFromCAM(0);
	cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_WIDTH, 320 );

	cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_HEIGHT, 240 );
	
	return capture;
}