//
// Inspiration: http://stackoverflow.com/questions/18108932/linux-c-serial-port-reading-writing
//
//

//
// Dependencies
//
#ifndef TRSERIAL_H
#define TRSERIAL_H

#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <stdint.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions

#define BBB_SERIAL "/dev/ttyO1"

/**
	These defines determine which PWM channel is connected as the azimuth channel and which channel controls the elevation motor.
*/
#define AZIMUTH_MOTOR 1
#define ELEV_MOTOR 2

/**
	@func trSerInit is used to initialize the serial port of the BBB. It sets the baud rate, the stop bits and other POSIX specific parameters.
*/
void trSerInit(void);
/**
	@func trSetSpeed sets the motor position for the specfied motor.
	@param motor. The motor parameter selects the motor channel which we wish to set the position to.
	@param sp is the position of the motor. The dynamic range of the parameter is determined by the defines MAX_AZI / MIN_AZI for azimuth and MAX_ELEV and MIN_ELEV for the elevation.
	
*/
void trSetSpeed(uint8_t motor, int16_t sp);

/** The function @func trWrite writes a general string to the opened serial port

*/

void trWrite(char* str);

#endif //TRSERIAL_H