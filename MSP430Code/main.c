// -----------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------
#include "system.h"
#include "uart.h"
#include "LaunchPadLed.h"
#include "LaunchPadPWM.h"

// -----------------------------------------------------------------------
// Structs, arrays, and user-defined data types
// -----------------------------------------------------------------------
buffer_t rx;
buffer_t tx;
char rx_buffer_array[UART_RX_BUFFER_LENGTH];
char tx_buffer_array[UART_TX_BUFFER_LENGTH];
enum com_state {IDLE, WRITING};
char receive_buffer[RECEIVE_MAX_LENGTH];

// -----------------------------------------------------------------------
// Forward Declarations
// -----------------------------------------------------------------------
void Process_Input(void);
void Process_Char(char c);
void Process_Cmd(char* cmd_str);
unsigned int Cmd_Str2Int(char* c_array);

// -----------------------------------------------------------------------
// Function Main
// -----------------------------------------------------------------------
int main(void)
{
	WDTCTL = WDTPW + WDTHOLD;
	DCOCTL  = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;
	BufferInit(&rx, &rx_buffer_array[0], UART_RX_BUFFER_LENGTH);
	UART_Init(0, &rx, &tx, UART_BAUD);
	ConfigureLED_Outputs();
	PWM_Init();
	__bis_SR_register(GIE);
	
	GreenLED_Off();
	RedLED_Off();

	while(1) {
		Process_Input();
		__delay_cycles(STEP_DELAY);
	}
}

// -----------------------------------------------------------------------
// Function Process_Input
// -----------------------------------------------------------------------
void Process_Input(void)
{
	char c;
	if(GetSize(&rx)) {
		GreenLED_On();
		c = Pop(&rx);
		Process_Char(c);
	}

}

// -----------------------------------------------------------------------
// Function Process_Char
// -----------------------------------------------------------------------
void Process_Char(char c)
{
    static enum com_state receive_state = IDLE;
    static int length;

    if (receive_state == IDLE) {
        if (c == RECEIVE_START_CHAR) {
            receive_state = WRITING;
            length = 0;
        }
    } else {
        receive_buffer[length] = c;
        if (c == RECEIVE_STOP_CHAR) {
            // process received message
            Process_Cmd(&receive_buffer[0]);
            receive_state = IDLE;
        } else {
            length++;
            if(length > RECEIVE_MAX_LENGTH) receive_state = IDLE;
        }
    }
}

// -----------------------------------------------------------------------
// Function Process_Cmd
// -----------------------------------------------------------------------
void Process_Cmd(char* cmd_str)
{
	unsigned int cmd_pwm = SERVO_MIN;
	unsigned int i;
	for(i = 1; i < Cmd_Str2Int(&cmd_str[1]); i++) {
		cmd_pwm += SV_STEP;
	}

	switch(cmd_str[0]) {
		case '1':
			RedLED_On();
			PWM_set_duty1(cmd_pwm);
			break;

		case '2':
			RedLED_Off();
			PWM_set_duty2(cmd_pwm);
			break;

		default:
			break;
	}
}

// -----------------------------------------------------------------------
// Function CmdStr2Int
// -----------------------------------------------------------------------
unsigned int Cmd_Str2Int(char* c_array)
{
    if((c_array[2] >= '0' && c_array[2] <= '9') && (c_array[1] >= '0' && c_array[1] <= '9') && (c_array[0] >= '0' && c_array[0] <= '9')) {
        return (((c_array[0]-'0')*100) + ((c_array[1]-'0')*10) + (c_array[2]-'0'));
    } else if((c_array[1] >= '0' && c_array[1] <= '9') && (c_array[0] >= '0' && c_array[0] <= '9')) {
        return (((c_array[0]-'0')*10) + (c_array[1]-'0'));
    } else if(c_array[0] >= '0' && c_array[0] <= '9') {
        return (c_array[0]-'0');
    } else {
        return 999;
    }
}
