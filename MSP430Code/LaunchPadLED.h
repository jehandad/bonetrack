#ifndef LAUNCHPADLED_H_
	#define LAUNCHPADLED_H_
	#include <msp430.h>
	// set pin 6 and pin 0 as output, all others input
	//#define ConfigureLED_Outputs() P1DIR |= BIT0; P1DIR |= BIT6;
	//#define ConfigureLED_Outputs() P1DIR = 0x41 // set pin 6 and pin 0 as output, all others input
	#define ConfigureLED_Outputs() P1DIR |= (BIT0 + BIT6) // Set the LEDs on P1.0, P1.6 as outputs
	#define RedLED_On() P1OUT |= BIT0
	#define RedLED_Off() P1OUT &= ~BIT0
	#define RedLED_Toggle() P1OUT ^= BIT0
	#define GreenLED_On() P1OUT |= BIT6
	#define GreenLED_Off() P1OUT &= ~BIT6
	#define GreenLED_Toggle() P1OUT ^= BIT6
	#define LEDsOff() P1OUT &= ~(BIT0 | BIT6)
	#define LEDsOn() P1OUT |= BIT0 | BIT6
	#define LEDs_Toggle() P1OUT ^= BIT0 + BIT6
#endif /* LAUNCHPADLED_H_ */
