#ifndef LAUNCHPADPWM_H_
	#define LAUNCHPADPWM_H_
	#include <msp430.h>
	#include "system.h"
	#define PWM_configure_outputs() P2DIR = BIT2 | BIT4; P2SEL = BIT2 | BIT4
	#define PWM_configure_TimerA1() TA1CCR0 = PWM_Period - 1;\
			TA1CTL = TASSEL_2 + MC_1; TA1CCTL1 = OUTMOD_7; TA1CCTL2 = OUTMOD_7
	#define PWM_set_duty1(D) TA1CCR1 = D
	#define PWM_set_duty2(D) TA1CCR2 = D
	#define PWM_Init() PWM_configure_outputs(); PWM_configure_TimerA1();
#endif /* LAUNCHPADPWM_H_ */
