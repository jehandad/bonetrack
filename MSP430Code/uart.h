/**
 * @defgroup uart UART Module
 * @file uart.h
 *
 *  Created on: Mar 12, 2014
 *      Author: Michael
 * @{
 */

#ifndef UART_H_
#define UART_H_
#include "buffer.h"
#include "system.h"

/** Initialize UART module
 *
 * Example usage:
 * @code
 * UART_Init(UART0_BASE, &rx0, &tx0, 115200);
 * @endcode
 *
 * @param base - Memory base of UART module if system supports multiple modules, input 0 otherwise
 * @param rx - pointer to receive buffer of type #buffer_t
 * @param tx - pointer to transmit buffer of type #buffer_t
 * @param baud - desired baud rate
 */
void UART_Init(uint32_t base, buffer_t* rx, buffer_t* tx, uint32_t baud);
// processor specific defines
#define NUM_UARTS 1
#define UART0_BASE 0
#define UART_InitHardware(base) P1SEL = BIT1 + BIT2 ; P1SEL2 = BIT1 + BIT2 // P1.1 = RXD, P1.2=TXD
#define UART_Disable(base) UCA0CTL1 = 0
#define UART_SetBaud(base,baud) UCA0BR0 = PERIPHERAL_CLOCK / baud; \
	UCA0BR1 = PERIPHERAL_CLOCK / baud / 256; \
	UCA0MCTL = ((PERIPHERAL_CLOCK*8) / baud - (PERIPHERAL_CLOCK / baud) * 8) << 1
#define UART_Configure(base) UCA0CTL1 |= UCSSEL_2
#define UART_EnableInterrupts(base) IE2 |= UCA0RXIE
#define UART_TX_ISR(void) _Pragma("vector=USCIAB0TX_VECTOR") \
		__interrupt void _UART_TX_ISR(void)
#define UART_RX_ISR(void) _Pragma("vector=USCIAB0RX_VECTOR") \
		__interrupt void _UART_RX_ISR(void)
#define UART_TX_Char(base,c) UCA0TXBUF = c
#define UART_RX_Char(base) UCA0RXBUF
#define READ_UART_TX_IF(base) ((IFG2 & 0x02) >> 1)
#define CLEAR_UART_TX_IF(base) IFG2 &= ~UCA0TXIFG
#define CLEAR_UART_RX_IF(base) IFG2 &= ~UCA0RXIFG
#define CLEAR_UART_TX_IE(base) IE2 &= ~UCA0TXIE
#define SET_UART_TX_IE(base) IE2 |= UCA0TXIE
#define UART_CAN_TRANSMIT(base) (IFG2 & UCA0TXIFG)
#define UART_DATA_AVAILABLE(base) (IFG2 & UCA0RXIFG)

/** @}*/
#endif /* UART_H_ */
